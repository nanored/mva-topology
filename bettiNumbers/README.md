# Betti Numbers

The content of this folder has been created following this tutorial https://topology-tool-kit.github.io/bettiNumbers.html.

Download all data needed at https://topology-tool-kit.github.io/stuff/bettiData.tar.gz and put the data in this folder.  

## State
You can run the state contained in [state.pvsm](state.pvsm) in paraview to observe the structure of the skull.

## Module
In your TTK folder you can run
```bash
scripts/createTTKmodule.sh BettiNumbers
```
Then just replace the file `core/base/bettiNumbers/BettiNumbers.h` of your TTK source-code folder with the file [BettiNumbers.h](BettiNumbers.h). After that you can recompile TTK whith the commands:
```bash
cd build
cmake ..
make -jN
sudo make install
```
Where `N` is the number of core  you want to use for the compilation.
Then if you run paraview you will be able to use the new filter BettiNumbers.

## C++

With TTK correctly installed on your computer, you can compile the code in the file [main.cpp](main.cpp) using the following commands (adapt the path of ttk in your case).
```bash
mkdir build
cd build
cmake .. -DTTKVTK_DIR="/usr/local/lib/cmake/ttk/"
make
```

The executable produced allow you to test the code to compute betti numbers without launching paraview. Furthermore the compilation is faster than the one of the module. To run the program just use the command:
```bash
./betti -i ../skull.vtu
```